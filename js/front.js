(function ($) {
  Drupal.behaviors.initSlider = {
    attach: function (context, settings) {
      $(document).ready(function () {
        $(".tarkikComandSlider").slick({
          rows : 2,
		  slidesToShow: 4,
          // slidesToScroll: 4,
          autoplay: false,
          // autoplaySpeed: 5000,
		  // arrows: true,
        });
      });
    },
  };
})(jQuery);