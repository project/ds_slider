<?php

namespace Drupal\ds_slider\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'DS Slider' block.
 *
 * @Block(
 *   id = "my_template_block",
 *   admin_label = @Translation("DS Slider")
 * )
 */
class DsSliderBlock extends BlockBase
{

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration()
  {
    return ['label_display' => FALSE];
  }

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    return [
      '#theme' => 'ds_slider',
      '#sliders' => "Fetching slider",
      '#cache' => array(
        'max-age' => 0,
      ),
    ];
  }
}
