<?php

namespace Drupal\ds_slider\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller routines for products routes.
 */
class DsSliderController extends ControllerBase
{
    public function content()
    {
        return [
            '#theme' => 'ds_slider',
            '#sliders' => "Slider Called from Controller!",
            '#cache' => array(
                'max-age' => 0,
            ),
        ];
    }
}
